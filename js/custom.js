// here we draw the charts to be displayed using canvas plugin Chart.js


//  donout chat
var ctx = document.getElementById("remaining-work-chart");

// And for a doughnut chart
var myDoughnutChart = new Chart(ctx, {
    type: 'doughnut',
       data: {
            datasets: [{
                data: [
                    20,
                    7,
                ],
                backgroundColor: [
                    '#5783e3',
                    '#ddd',
                ],
                label: 'Dataset 1'
            }],
            labels: [
                "Blue",
                "Gray",
            ]
        },
        options: {
            responsive: true,
            legend: {
                display: false,
                position: 'top',
            },
            title: {
                display: false,
                text: 'Chart.js Doughnut Chart'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            },
            cutoutPercentage: 85
        }
});


// second donout chart
var ctx2 = document.getElementById("reviews-chart");

// And for a doughnut chart
var myDoughnutChart = new Chart(ctx2, {
    type: 'doughnut',
       data: {
            datasets: [{
                data: [
                    10,
                    10,
                ],
                backgroundColor: [
                    '#feb13b',
                    '#ddd',
                ],
                label: 'Dataset 2'
            }],
            labels: [
                "Orange",
                "Gray",
            ]
        },
        options: {
            responsive: true,
            legend: {
                display: false,
                position: 'top',
            },
            title: {
                display: false
            },
            animation: {
                animateScale: true,
                animateRotate: true
            },
            cutoutPercentage: 85
        }
});

// third dounout chart
var ctx3 = document.getElementById("goals-chart");

// And for a doughnut chart
var myDoughnutChart = new Chart(ctx3, {
    type: 'doughnut',
       data: {
            datasets: [{
                data: [
                    5,
                    2,
                ],
                backgroundColor: [
                    '#fb7474',
                    '#ddd',
                ],
                label: 'Dataset 3'
            }],
            labels: [
                "Red",
                "Gray",
            ]
        },
        options: {
            responsive: true,
            legend: {
                display: false,
                position: 'top',
            },
            title: {
                display: false
            },
            animation: {
                animateScale: true,
                animateRotate: true
            },
            cutoutPercentage: 85
        }
});

// line/area chart
var ctx5 = document.getElementById("statistic-chart");

var myLineChart = new Chart(ctx5, {
    type: 'line',
    data:{
    labels : [" ","SUN","MON","TUE","WED","THU","FRI","SAT"," "],
    datasets : [{
        fillColor : "#ebf2ff",
        fill: 'origin',
        strokeColor : "rgba(220,220,220,1)",
        borderColor: "#6192f9",
        cubicInterpolationMode: 'monotone',
        pointColor : "rgba(220,220,220,1)",
        pointStrokeColor : "#fff",
        lineTension: 2,
        data : [0,2,7,5,15,4,0,0,0]
    }],

    },
    options: {
        scaleSteps: 10,
        scales: {
            yAxes: [{
                ticks: {
                  min: 0,
                  max: 30,
                  stepSize: 5,
                }
            }],
            xAxes: [{
                color: '#ddd'
               
            }]
        },
          legend: {
            display: false
         }
    }
});